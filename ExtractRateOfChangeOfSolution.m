function [dz,z]=ExtractRateOfChangeOfSolution(t,z,t0)

% uses interpolation to extract rate of change of z at time t0 from solution
% for finding velocity, angular velocity and force

dt=1e-5;

n=size(z,2);
[T,NP]=ndgrid(t,1:n);

T0=repmat(t0,1,n);

Z=griddedInterpolant(T,NP,z);
dz=(Z(T0+dt,1:n)-Z(T0-dt,1:n))/(2*dt);
z=Z(T0,1:n);
