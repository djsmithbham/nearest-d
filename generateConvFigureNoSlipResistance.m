% generate results no-slip resistance

clear all;

a=1;
m=[5 10 15 20];
q=[10 15 20 30 40 60 80];

U=[1;0;0];
ep=0.01;
domain='i';
blockSize=0.2;
nlocalpts=9;

for iM=1:length(m)
    ['m = ', num2str(m(iM))]
    for iQ=1:length(q)
        [x,X]=GenerateSpherePoints(m(iM),q(iQ),a);
        F=SolveSingleLayerNoSlipResistance(x,X,U,ep,domain,blockSize);
        err(iM,iQ)=(norm(F-[6*pi*a;0;0])/(6*pi*a));
        hq(iQ)=CalcDiscr_h(X,blockSize);
        hf(iM)=CalcDiscr_h(x,blockSize);
        Dof(iM)=length(x);
        NM(iM)=length(x)/3;
        NQ(iQ)=length(X)/3;
    end
end

%%
% load figure 13a.mat
wd=8;ht=5;
fs=9;fn='times';

iM=4;
figure(1);clf;
loglog(hq,err(iM,:));
hx=xlabel('\(h_q\)','interpreter','latex');
hy=ylabel(['relative error with \(' num2str(Dof(iM)) '\) DoF'],'interpreter','latex');
set(gcf,'paperunits','centimeters');
set(gcf,'papersize',[wd ht]);
set(gcf,'paperposition',[0 0 wd ht]);
box on;
set(gca,'tickdir','out');
set(hx,'fontsize',fs,'fontname',fn);
set(hy,'fontsize',fs,'fontname',fn);
set(gca,'fontsize',fs,'fontname',fn);
print(gcf,'-dpdf','-r600','noSlipConvVariedhq.pdf');

