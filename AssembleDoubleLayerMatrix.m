function [A,NN]=AssembleDoubleLayerMatrix(xCollNodes,xQuadNodes,xVelNodes,NML,dS,ep,domain,blockSize,corrector)

	% blockSize is in GB
	% stokeslet values are calculated in blocks to avoid memory overflow
	% each block is multiplied by the nearest neighbour matrix 
	% the result is summed to yield the stokeslet matrix
	% NML = normal at quadrature points
	% dS = metric at quadrature points
	% corrector determines whether to use the RBM corrector trick for inner integrals on surface, values 0 or 1

	M=length(xCollNodes)/3;
	Q=length(xQuadNodes)/3;
    N=length(xVelNodes)/3;

	%find closest force node to each quadrature node
	NN=NearestNeighbourMatrix(xQuadNodes,xVelNodes,blockSize);

	% calculate number of quadrature nodes that can be used for each block
	blockNodes=floor(blockSize*2^27/(9*M));

	%assemble double layer matrix 
	A=zeros(3*M,3*N);
	for iMin=1:blockNodes:Q
		iMax=min(iMin+blockNodes-1,Q);
		iRange=[iMin:iMax Q+iMin:Q+iMax 2*Q+iMin:2*Q+iMax];
        % minus sign here arises due to swapping order of arguments to
        % stress
		T=-RegStokesletStress(xCollNodes,xQuadNodes(iRange),ep);
		N1=repmat(NML(iMin:iMax,1)',3*M,3);
		N2=repmat(NML(iMin:iMax,2)',3*M,3);
		N3=repmat(NML(iMin:iMax,3)',3*M,3);
		A=A+(T(:,:,1).*N1+T(:,:,2).*N2+T(:,:,3).*N3)*(repmat(dS(iMin:iMax)',3,size(NN,2)).*NN(iRange,:));
	end
	if corrector
        % extract blocks
        Axx=A(      1:M,      1:N);
        Axy=A(      1:M,  N+1:2*N);
        Axz=A(      1:M,2*N+1:3*N);
        Ayx=A(  M+1:2*M,      1:N);
        Ayy=A(  M+1:2*M,  N+1:2*N);
        Ayz=A(  M+1:2*M,2*N+1:3*N);
        Azx=A(2*M+1:3*M,      1:N);
        Azy=A(2*M+1:3*M,  N+1:2*N);
        Azz=A(2*M+1:3*M,2*N+1:3*N);
        % zero inner integral
        Axx(1:N+1:end)=0;
        Axy(1:N+1:end)=0;
        Axz(1:N+1:end)=0;
        Ayx(1:N+1:end)=0;
        Ayy(1:N+1:end)=0;
        Ayz(1:N+1:end)=0;
        Azx(1:N+1:end)=0;
        Azy(1:N+1:end)=0;
        Azz(1:N+1:end)=0;
        Axx(1:N+1:end)=-sum(Axx,2)+0.5*ones(M,1);
        Axy(1:N+1:end)=-sum(Axy,2);
        Axz(1:N+1:end)=-sum(Axz,2);
        Ayx(1:N+1:end)=-sum(Ayx,2);
        Ayy(1:N+1:end)=-sum(Ayy,2)+0.5*ones(M,1);
        Ayz(1:N+1:end)=-sum(Ayz,2);
        Azx(1:N+1:end)=-sum(Azx,2);
        Azy(1:N+1:end)=-sum(Azy,2);
        Azz(1:N+1:end)=-sum(Azz,2)+0.5*ones(M,1);
        A=[Axx Axy Axz; Ayx Ayy Ayz; Azx Azy Azz];
	end


