function [x,v,X,XCL]=UndulatoryModelFiniteThicknessIncludedPts(t,model)

% generates discretisation of a model undulatory swimmer,
% griddedInterpolant flagellum, finite thickness (i.e. surface
% discretisation)
%
% t - time
% model.ns - number of points along flagellum
% model.F  - F{1} is x-interpolant, F{2} is y-interpolant

%-------------------------------------------------------------------
% coarse grid - position and velocity
% xCL is centreline

% flagellum
s=linspace(0,1,model.ns+1);
ds=s(2)-s(1);
s=s(1:end-1)+ds*0.5;
% finite differences for velocity... can't be too precise as x,y are
% outputs of fsolve
dt=0.001;
tt=[t-dt/2  t  t+dt/2];

[sc,tc]=ndgrid(s,tt); % data goes in model.F
[xc,yc]=CalcxyFromPlanarInterp(sc,tc,model.F);

ag=model.arad.*model.thicknessFn(sc);  % radius times thickness function

[~,normVec,binormVec]=CalculateBasisAlongPlanarCurve(sc,xc,yc);

% number of theta points to reflect tapering of the radius
for js=1:model.ns
    nth=ceil(model.nth*model.thicknessFn(s(js))); % reduces no. theta points when radius significantly less than max
    th=linspace(0,2*pi,nth+1);th=th+0.5*(th(2)-th(1));
    th(end)=[];

    thh=repmat(th,3,1); %repmat(thg,length(s),3); % three time values evaluated for finite difference

    aa=repmat(squeeze(ag(js,:))',1,nth);
    
    xx=repmat(squeeze(xc(js,:))',1,nth);
    yy=repmat(squeeze(yc(js,:))',1,nth);

    nx=repmat(squeeze(normVec.x(js,:))',1,nth);
    ny=repmat(squeeze(normVec.y(js,:))',1,nth);
    nz=repmat(squeeze(normVec.z(js,:))',1,nth);

    bx=repmat(squeeze(binormVec.x(js,:))',1,nth);
    by=repmat(squeeze(binormVec.y(js,:))',1,nth);
    bz=repmat(squeeze(binormVec.z(js,:))',1,nth);

    x1{js}   =   xx + nx.*cos(thh).*aa + bx.*sin(thh).*aa;
    x2{js}   =   yy + ny.*cos(thh).*aa + by.*sin(thh).*aa;
    x3{js}   =        nz.*cos(thh).*aa + bz.*sin(thh).*aa;

end

xg1=[];xg2=[];xg3=[];
vg1=[];vg2=[];vg3=[];
for js=1:model.ns
    xg1=[xg1;reshape(x1{js}(2,:),[],1)];
    xg2=[xg2;reshape(x2{js}(2,:),[],1)];
    xg3=[xg3;reshape(x3{js}(2,:),[],1)];
    vg1=[vg1;reshape(x1{js}(3,:),[],1)-reshape(x1{js}(1,:),[],1)];
    vg2=[vg2;reshape(x2{js}(3,:),[],1)-reshape(x2{js}(1,:),[],1)];
end
vg1=vg1/dt;vg2=vg2/dt;vg3=vg1*0;    

x=[xg1;xg2;xg3];
v=[vg1;vg2;vg3];

%-------------------------------------------------------------------------
% fine grid - position only
%
% flagellum
model.Ns=model.ns*3+1;
S=linspace(0,1,model.Ns);

[Sc,tc]=ndgrid(S,tt(:,2)); % 'central' t value only, so Sc and tc are Ns x 1
[Xc1,Xc2]=CalcxyFromPlanarInterp(Sc,tc,model.F);
A=model.arad.*model.thicknessFn(Sc);  % radius times thickness function

[~,NormVec,BinormVec]=CalculateBasisAlongPlanarCurve(Sc,Xc1,Xc2);

for js=1:model.Ns
    nth=ceil(model.nth*model.thicknessFn(S(js)));
    Nth=nth*3+1;
    Th=linspace(0,2*pi,Nth+1);
    Th(end)=[];
%    Thh=repmat(Th,3,1); %repmat(thg,length(s),3); % three time values evaluated for finite difference

    AA=repmat(A(js),1,Nth);
    
    XX1=repmat(Xc1(js),1,Nth);
    XX2=repmat(Xc2(js),1,Nth);

    Nx=repmat(NormVec.x(js),1,Nth);
    Ny=repmat(NormVec.y(js),1,Nth);
    Nz=repmat(NormVec.z(js),1,Nth);

    Bx=repmat(BinormVec.x(js),1,Nth);
    By=repmat(BinormVec.y(js),1,Nth);
    Bz=repmat(BinormVec.z(js),1,Nth);

    X1{js}   =   XX1 + Nx.*cos(Th).*AA + Bx.*sin(Th).*AA;
    X2{js}   =   XX2 + Ny.*cos(Th).*AA + By.*sin(Th).*AA;
    X3{js}   =         Nz.*cos(Th).*AA + Bz.*sin(Th).*AA;

end

Xg1=[];Xg2=[];Xg3=[];
for js=1:model.Ns
    Xg1=[Xg1;X1{js}(:)];
    Xg2=[Xg2;X2{js}(:)];
    Xg3=[Xg3;X3{js}(:)];
end

X =[Xg1;Xg2;Xg3];

X=MergeVectorGrids(X,x);

XCL=[Xc1(:);Xc2(:);0*Xc1(:)];
