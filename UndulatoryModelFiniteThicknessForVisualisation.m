function [x,y,z]=UndulatoryModelFiniteThicknessForVisualisation(t,model)

% generates discretisation of a model undulatory swimmer,
% griddedInterpolant flagellum, finite thickness (i.e. surface
% discretisation) for surface visualisation
%
% t - time
% model.ns - number of points along flagellum
% model.F  - F{1} is x-interpolant, F{2} is y-interpolant

%-------------------------------------------------------------------
% coarse grid - position and velocity
% xCL is centreline

% flagellum
s=linspace(0,1,model.Ns);
ds=s(2)-s(1);
[S,T]=ndgrid(s,t);

[xc,yc]=CalcxyFromPlanarInterp(S,T,model.F);

ag=model.arad.*model.thicknessFn(s);  % radius times thickness function
size(S)
[~,normVec,binormVec]=CalculateBasisAlongPlanarCurve(S,xc,yc);

th=linspace(0,2*pi,model.Nth);

x = xc(:) + ag(:).*normVec.x(:)*cos(th) + ag(:).*binormVec.x(:)*sin(th);
y = yc(:) + ag(:).*normVec.y(:)*cos(th) + ag(:).*binormVec.y(:)*sin(th);
z =         ag(:).*normVec.z(:)*cos(th) + ag(:).*binormVec.z(:)*sin(th);

