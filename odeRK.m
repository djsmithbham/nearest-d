function [t,z]=odeRK(f,tRange,z0,varargin)

if ~isempty(varargin)
    dt=varargin{1};
else
    dt=0.05;
end

t=tRange(1):dt:tRange(2);

z=zeros(length(t),length(z0));
z(1,:)=z0;
for n=1:length(t)-1
    k1=f(t(n),z(n,:))';
    tm=0.5*(t(n)+t(n+1));
    zm1=z(n,:)+dt*k1*0.5;
    k2=f(tm,zm1)';
    zm2=z(n,:)+dt*k2*0.5;
    k3=f(tm,zm2)';
    z3=z(n,:)+dt*k3;
    k4=f(t(n+1),z3)';
    z(n+1,:)=z(n,:)+(1/6)*dt*(k1+2*k2+2*k3+k4);    
end

