% generate all tables and figures
%
% to shortcut calculations and plot immediately, 
%     uncomment the load statement in each script file and
%     run the section

clear all;

% figure 1: accuracy of metric approximation
testAndPlotMetricApprox;

%%
% figure 2a: flow field due to translating sphere (no-slip)
generateFlowFieldNoSlipResistance;

%%
% figure 2b: flow field due to translating sphere (free-slip)
generateFlowFieldSlipResistance;

%%
% figure 3: partial slip resistance problem
generatePartialSlipResistanceFigure;

%%
% figure 4a: slip velocity squirmer velocity field
generateFlowFieldSlipSquirmer;

%%
% figure 4b: slip velocity squirmer timings; 
% figure 14ab: slip velocity squirmer convergence
generateConvFigureSlipSquirmer;

%%
% slip velocity squirmer naive calculation
generateResultNaiveSLSquirmer;

%%
% figure 5: undulatory swimmer normal
% figure 6: undulatory swimmer flow field top view and trajectory - slender
% figure 7: undulatory swimmer flow field top view and trajectory - fat
% figure 8: undulatory swimmer flow field isometric view - slender
% figure 9: undulatory swimmer flow field isometric view - fat
generateSwimmerFlowPlotDL;

%%
% figure 10: undulatory swimmer flow field isometric view - slender
% figure 11: undulatory swimmer flow field isometric view - fat
generateSwimmerFlowPlotBdryDL;

%%
% figure 12: undulatory swimmer SL vs DL
testRoWSweepDL;
testRoWSweepSL;
generateSLDLUndulatoryComparison;



%%
% figure 13a: convergence of sphere resistance problem, no-slip
generateConvFigureNoSlipResistance;

%%
% figure 13b: convergence of sphere resistance problem, free-slip
generateConvFigureSlipResistance;