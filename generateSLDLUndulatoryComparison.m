% plot SL and DL results

load('figure12a.mat');
load('figure12b.mat');

%%
wd=8;ht=7;
opts.fig=1;
opts.fs=10;
opts.fn='times';
     
figure(opts.fig);clf;hold on;
plot(aVals,RoWSL,'r--');
plot(aVals,RoWDL,'b-');
hx=xlabel('\(a_0\)','interpreter','latex');set(hx,'fontsize',opts.fs,'fontname',opts.fn);
hy=ylabel('\(\bar{\mathcal{W}}\)','interpreter','latex');set(hx,'fontsize',opts.fs,'fontname',opts.fn);
set(gca,'fontsize',opts.fs,'fontname',opts.fn);
box on;
ylim([0.6 1.2]);
set(gca,'tickdir','out');
hl=legend('SLP only, \(\mathcal{W}_{q}\)','SLP+DLP, \(\mathcal{W}_{f}\)','interpreter','latex');
set(hl,'box','off');
set(opts.fig,'paperunits','centimeters','papersize',[wd ht],'paperposition',[0 0 wd ht]);
print(opts.fig,'-r600','-dpdf','undulatorySweepSLDLRoW.pdf');
