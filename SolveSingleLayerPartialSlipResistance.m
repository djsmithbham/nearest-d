function [FF,F,us,w,NN]=SolveSingleLayerPartialSlipResistance(x,X,U,lambda,ep,domain,blockSize,nlocalpts)

% F is stokeslet distribution (=-f)
% us is surface velocity distribution

M=length(x)/3;
Q=length(X)/3;

[tgt1,tgt2,nml,~]=GenerateTangentNormalAndMetric(x,nlocalpts,[0,0,0]);
[~,~,NML,dS]=GenerateTangentNormalAndMetric(X,nlocalpts,[0,0,0]);  

[AS,NN]=AssembleStokesletMatrix(x,X,x,ep,domain,blockSize);
u=kron(U,ones(M,1));

tgt1mat=[diag(tgt1(1:M)) diag(tgt1(M+1:2*M)) diag(tgt1(2*M+1:3*M))];
tgt2mat=[diag(tgt2(1:M)) diag(tgt2(M+1:2*M)) diag(tgt2(2*M+1:3*M))];
tgtmat=[tgt1mat; tgt2mat];
nmlmat=[diag(nml(1:M)) diag(nml(M+1:2*M)) diag(nml(2*M+1:3*M))];

% quick solution just for force
%rhs=[nmlmat*u; zeros(2*M,1)];
%A=[-nmlmat*inv(0.5*eye(3*M)+AT)*AS; tgtmat];   % slightly over-complicated and we need u on surface anyway
%f=A\rhs;

% need metric to relate F (stokeslet strength) to f (traction)
% calculated by projecting dS from quadrature to force points, then
% dividing out number of nearest neighbours
dSForcePts=repmat(([dS dS dS]*NN)./sum(NN,1),2*M,1); 

A = [ -AS,                       -eye(3*M); ...      % AS not -AS because of sign on F_k
      zeros(M,3*M),               nmlmat  ; ...
     -lambda*tgtmat./dSForcePts,  tgtmat];           % relate tangential traction to tangential velocity
rhs=[zeros(3*M,1) ; ...
      nmlmat*u     ; ...
      tgtmat*u];
sol=A\rhs;

F=-sol(1:3*M);
us=sol(3*M+1:6*M);
w=sum(NN,1);

[F1,F2,F3]=ExtractComponents(NN*F);
FF=[sum(F1);sum(F2);sum(F3)];
