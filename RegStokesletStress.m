function T=RegStokesletStress(x,X,ep)
	% x is a vector of field points:  3*M
	% X is a vector of source points: 3*Q
	% ep is regularisation parameter
	% outputs an array of regularised stokeslets stress (Txyz) between field and source points
	% blocks are [Txxx, Txyx, Txzx; Tyxx, Tyyx, Tyzx; Tzxx, Tzyx, Tzzx]
	%            [Txxy, Txyy, Txzy; Tyxy, Tyyy, Tyzy; Tzxy, Tzyy, Tzzy]
	%            [Txxz, Txyz, Txzz; Tyxz, Tyyz, Tyzz; Tzxz, Tzyz, Tzzz] where Txxx is M x Q etc. 
	x=x(:);
	X=X(:);
	M=length(x)/3;
	Q=length(X)/3;
	r1=      x(1:M)*ones(1,Q)-ones(M,1)*      X(1:Q)';
	r2=  x(M+1:2*M)*ones(1,Q)-ones(M,1)*  X(Q+1:2*Q)';
	r3=x(2*M+1:3*M)*ones(1,Q)-ones(M,1)*X(2*Q+1:3*Q)';
	rsq=r1.^2+r2.^2+r3.^2;
	irep5=1./(sqrt((rsq+ep^2)).^5);
	dyadic=-6.0*[r1.*r1 r1.*r2 r1.*r3; r2.*r1 r2.*r2 r2.*r3; r3.*r1 r3.*r2 r3.*r3];
	triadic1=dyadic.*kron(ones(3,3),r1.*irep5);
	triadic2=dyadic.*kron(ones(3,3),r2.*irep5);
	triadic3=dyadic.*kron(ones(3,3),r3.*irep5);
	correction1=-3.0*ep^2*(kron(eye(3),r1.*irep5)+kron([1;0;0],[r1 r2 r3].*[irep5 irep5 irep5]) ...
			      +kron([1 0 0],[r1;r2;r3].*[irep5;irep5;irep5]));
	correction2=-3.0*ep^2*(kron(eye(3),r2.*irep5)+kron([0;1;0],[r1 r2 r3].*[irep5 irep5 irep5]) ...
			      +kron([0 1 0],[r1;r2;r3].*[irep5;irep5;irep5]));
	correction3=-3.0*ep^2*(kron(eye(3),r3.*irep5)+kron([0;0;1],[r1 r2 r3].*[irep5 irep5 irep5]) ...
			      +kron([0 0 1],[r1;r2;r3].*[irep5;irep5;irep5]));
	T(:,:,1)=(1.0/(8.0*pi))*(triadic1+correction1);
	T(:,:,2)=(1.0/(8.0*pi))*(triadic2+correction2);
	T(:,:,3)=(1.0/(8.0*pi))*(triadic3+correction3);

