% generateConvFigureSlipSquirmer.m

a=1;
m=[5 10 15 20];
q=[10 15 20 30 45 60 90 120];
ep=0.01;
domain='i';
blockSize=0.2;
nlocalpts=9;
usl=3/2;

for iM=1:length(m)
    ['m = ', num2str(m(iM))]
    for iQ=1:length(q)
        tic;
        [x,X]=GenerateSpherePoints(m(iM),q(iQ),a);
        [U,Om,F,~,w,W]=SolveSlipSquirmerDL(x,X,usl,ep,domain,blockSize,nlocalpts);
        errU(iM,iQ)=norm(U-[0;0;-1]);
        errW(iM,iQ)=abs(W-12*pi)/(12*pi);
        hq(iQ)=CalcDiscr_h(X,blockSize);
        hf(iM)=CalcDiscr_h(x,blockSize);
        Dof(iM)=length(x);
        NM(iM)=length(x)/3;
        NQ(iQ)=length(X)/3;
        WT(iM,iQ)=toc;
    end
end

%%
% load figure4b14ab.mat

wd=8;ht=5;
fs=9;fn='times';

% figure 14ab

iM=4;
figure(1);clf;
loglog(hq,errU(iM,:));
hx=xlabel('\(h_q\)','interpreter','latex');
hy=ylabel(['rel. error in \(\mathcal{U}\); \(' num2str(Dof(iM)) '\) DoF'],'interpreter','latex');
set(gcf,'paperunits','centimeters');
set(gcf,'papersize',[wd ht]);
set(gcf,'paperposition',[0 0 wd ht]);
box on;
set(gca,'tickdir','out');
set(hx,'fontsize',fs,'fontname',fn);
set(hy,'fontsize',fs,'fontname',fn);
set(gca,'fontsize',fs,'fontname',fn);
print(gcf,'-dpdf','-r600','slipSwimmerConvUVariedhq.pdf');

figure(1);clf;
loglog(hq,errW(iM,:));
hx=xlabel('\(h_q\)','interpreter','latex');
hy=ylabel(['rel. error in \(\mathcal{W}\); \(' num2str(Dof(iM)) '\) DoF'],'interpreter','latex');
set(gcf,'paperunits','centimeters');
set(gcf,'papersize',[wd ht]);
set(gcf,'paperposition',[0 0 wd ht]);
box on;
set(gca,'tickdir','out');
set(hx,'fontsize',fs,'fontname',fn);
set(hy,'fontsize',fs,'fontname',fn);
set(gca,'fontsize',fs,'fontname',fn);
print(gcf,'-dpdf','-r600','slipSwimmerConvWVariedhq.pdf');

% figure 4b

ht=8;

figure(1);clf;
hb=bar3(Dof(:),log10(WT));
hy=ylabel('DoF','interpreter','latex');
set(gca,'zticklabel',{'1','10','100','1000'});
hz=zlabel('walltime (s)','interpreter','latex');
hx=xlabel('\(h_q\)','interpreter','latex');
xl={};
for p=1:length(hq)
    xl{p}=num2str(hq(p));
end
view([-58 17]);
set(gca,'xticklabel',xl);
set(gca,'fontsize',fs,'fontname',fn);
set(hx,'fontsize',fs,'fontname',fn);
set(hy,'fontsize',fs,'fontname',fn);
set(hz,'fontsize',fs,'fontname',fn);
set(gcf,'paperunits','centimeters');
wd=9;ht=7;
set(gcf,'papersize',[wd ht]);
set(gcf,'paperposition',[0 0.4 wd ht]);
box on;
print(gcf,'-dpdf','-r600','slipSwimmerDoF.pdf');

