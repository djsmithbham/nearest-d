clear all;

a=1;
M=4;
Q=[5 10 15 20 30 40 50 60 75 90];
nlocalpts=[4 6 9 12 16];

for nlp=1:length(nlocalpts)
    for nq=1:length(Q)
        Q(nq)
        [~,X]=GenerateSpherePoints(M,Q(nq),a);
        [X1,X2,X3]=ExtractComponents(X);
        [~,~,NN,dS{nq}]=GenerateTangentNormalAndMetric(X,nlocalpts(nlp),[0;0;0]);
        err(nq,nlp)=(sum(dS{nq})-4*pi*a^2)/(4*pi*a^2);
        h(nq,nlp)=CalcDiscr_h(X,0.2);
    end
end

slope=(log10(err(2,1))-log10(err(end,1)))/(log10(h(2,1))-log10(h(end,1)))

%%

% load figure1.mat

wd=12;ht=7;fs=9;fn='times';
figure(1);clf;
loglog(h(:,1),abs(err(:,1)),'-ko','DisplayName','J=4');hold on;
loglog(h(:,2),abs(err(:,2)),'-b^','DisplayName','J=6');
loglog(h(:,3),abs(err(:,3)),'-r*','DisplayName','J=9');
h4=loglog(h(:,4),abs(err(:,4)),'-x','DisplayName','J=12');
set(h4,'color',[0 0.4 0]);
loglog(h(:,5),abs(err(:,5)),'-m+','DisplayName','J=16');
%loglog(h(:,6),abs(err(:,6)),'-cd','DisplayName','J=4');
%hl=legend(hh,'J=4','J=6','J=9','J=12','J=16','J=25');
xlim([0.01 1]);
hl=legend;
set(hl,'location','eastoutside');
hx=xlabel('\(h\), point spacing','interpreter','latex','fontsize',fs);
hy=ylabel('rel. err.','interpreter','latex','fontsize',fs);
set(gca,'fontsize',fs);
set(gca,'fontname',fn);
box on;
set(gca,'tickdir','out');
set(1,'paperunits','centimeters','papersize',[wd ht],'paperposition',[0 0 wd ht]);
print(1,'-dpdf','AreaVs_h_Error.pdf');