function [Theta,tNth]=CalcPolarAngleSphere(x)

% calculates polar angle for point on the surface of sphere
%     angle is relative to north pole
% and north-pointing tangent vector

[x1,x2,x3]=ExtractComponents(x);

% check whether supplied points are a sphere centred at origin
a=sqrt(x1.^2+x2.^2+x3.^2);
if var(a)>1e-9
    ['not a sphere centred at origin, variance of distance from origin' num2str(var(a))]
    pause
end
N=length(x1);
x1=x1./a;
x2=x2./a;
x3=x3./a;

Theta=real(acos(x3)); % polar angle - taking real part as floating point error can give small erroneous imaginary part
Theta=Theta(:);
Phi=(2*(x2>=0)-1).*real(acos(x1./sin(Theta))); % azimuthal angle
Phi(isnan(Phi))=0; % set Phi to zero at north and south pole
tNth=[-cos(Phi).*cos(Theta);-sin(Phi).*cos(Theta);sin(Theta)];
[t1,t2,t3]=ExtractComponents(tNth);

%figure(1);clf;hold on;
%plot3(x1,x2,x3,'.');
%quiver3(x1,x2,x3,t1,t2,t3);
%axis equal;

