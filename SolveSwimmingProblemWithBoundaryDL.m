function [dz,U,Om,F,W,NML]=SolveSwimmingProblemWithBoundaryDL(z,swimmer,boundary,t,epsilon,domain,blockSize,nlocalpts,varargin)

    % solves force-free swimming problem for translational and angular velocity U, Om
    % in the presence of a stationary rigid boundary (e.g. finite plane wall)
    % uses full boundary integral equation, including double layer potential
    %
    % input: z       - position/orientation of swimmer
    %                  dz(1:3) = x0      - origin of swimmer
    %                  dz(4:6) = b1      - first basis vector of swimmer frame
    %                  dz(7:9) = b2      - second basis vector of swimmer frame
    %        swimmer - structure describing how to construct swimmer
    %        xb      - discretisation of stationary boundary - force points
    %        Xb      - discretisation of stationary boundary - quadrature points
    %        t       - time (scalar)
    %
    % variables: U      - translational velocity
    %            Om     - angular velocity
    %
    % output:
    %         rigid body modes U, Om
    %         force exerted by nodes proximal to each force point F
    %         rate of working W
    %         normal NML
t
    x0=z(1:3);
    b1=z(4:6);
    b2=z(7:9);
    b3=cross(b1,b2);
    B=[b1(:) b2(:) b3(:)];

    [xi,vi,Xi,xCL]=swimmer.fn(t,swimmer.model);
    
    xx0=ApplyRotationMatrix(B,xi); % x-x0
    xs=TranslatePoints(xx0,x0);
    vs=ApplyRotationMatrix(B,vi); %
    XX0=ApplyRotationMatrix(B,Xi); % X-x0
    Xs=TranslatePoints(XX0,x0);
    
    xxCL=ApplyRotationMatrix(B,xCL); % interior points (centreline) for double layer
    Xint=TranslatePoints(xxCL,x0);

    NNss=NearestNeighbourMatrix(Xs,xs,blockSize);
    
    if isempty(boundary)
        xb=[];
        Xb=[];
        vb=[];
        NNbb=sparse([]);
    else
        [xb,Xb]=boundary.fn(boundary.model);
        vb=xb*0; % boundary is stationary
        NNbb=NearestNeighbourMatrix(Xb,xb,blockSize);
    end
    
    x=MergeVectorGrids(xs,xb);
    X=MergeVectorGrids(Xs,Xb);
    v=MergeVectorGrids(vs,vb);
    
    % only need DLP integrated over swimmer if boundary is rigid
    [~,~,NML,dS]=GenerateTangentNormalAndMetric(Xs,nlocalpts,Xint);
    
    NN=MergeNNMatrices(NNss,NNbb); % assemble nearest-neighbour matrices separately for swimmer and boundary
 
    Ns=length(xs)/3;
    Qs=length(Xs)/3;
    Nb=length(xb)/3;
    Qb=length(Xb)/3;
    
    N=Ns+Nb;
    Q=Qs+Qb;
    
    [AS,~]=AssembleStokesletMatrix(x,X,x,epsilon,domain,blockSize,NN);
    
    % double layer over swimmer only. Separate calculation for on-swimmer
    % and off-swimmer (use RBM corrector for on-swimmer)
    
    if isempty(boundary)
        AT=AssembleDoubleLayerMatrix(x,X,x,NML,dS,epsilon,domain,blockSize,1);
    else
        [ATs,~]=AssembleDoubleLayerMatrix(xs,Xs,xs,NML,dS,epsilon,domain,blockSize,1);
        [ATb,~]=AssembleDoubleLayerMatrix(xb,Xs,xs,NML,dS,epsilon,domain,blockSize,0);
        % interleave x,y,z components
        AT = [ATs(1:Ns,1:Ns)        zeros(Ns,Nb) ATs(1:Ns,Ns+1:2*Ns)        zeros(Ns,Nb) ATs(1:Ns,2*Ns+1:3*Ns)        zeros(Ns,Nb);...
              ATb(1:Nb,1:Ns)        zeros(Nb,Nb) ATb(1:Nb,Ns+1:2*Ns)        zeros(Nb,Nb) ATb(1:Nb,2*Ns+1:3*Ns)        zeros(Nb,Nb);...
              ATs(Ns+1:2*Ns,1:Ns)   zeros(Ns,Nb) ATs(Ns+1:2*Ns,Ns+1:2*Ns)   zeros(Ns,Nb) ATs(Ns+1:2*Ns,2*Ns+1:3*Ns)   zeros(Ns,Nb);...
              ATb(Nb+1:2*Nb,1:Ns)   zeros(Nb,Nb) ATb(Nb+1:2*Nb,Ns+1:2*Ns)   zeros(Nb,Nb) ATb(Nb+1:2*Nb,2*Ns+1:3*Ns)   zeros(Nb,Nb);...
              ATs(2*Ns+1:3*Ns,1:Ns) zeros(Ns,Nb) ATs(2*Ns+1:3*Ns,Ns+1:2*Ns) zeros(Ns,Nb) ATs(2*Ns+1:3*Ns,2*Ns+1:3*Ns) zeros(Ns,Nb);...
              ATb(2*Nb+1:3*Nb,1:Ns) zeros(Nb,Nb) ATb(2*Nb+1:3*Nb,Ns+1:2*Ns) zeros(Nb,Nb) ATb(2*Nb+1:3*Nb,2*Ns+1:3*Ns) zeros(Nb,Nb)];
    end
    
    AF = kron(eye(3),[sum(NNss(1:Qs,1:Ns),1) zeros(1,Nb)]); % force summation - only on swimmer
    
    AU = - kron(eye(3),[ones(Ns,1);zeros(Nb,1)]);
    
    [x1,x2,x3]=ExtractComponents(xx0);ze=0*x1; % component of velocity due to rotation of swimmer about x0; zero velocity of boundary
    AOm=[ze -x3 x2; zeros(Nb,3); x3 ze -x1; zeros(Nb,3); -x2 x1 ze; zeros(Nb,3)];

    [x1,x2,x3]=ExtractComponents(XX0'*NNss);ze=0*x1; % moment summation
    
    AM=[ ze zeros(1,Nb) -x3 zeros(1,Nb)  x2 zeros(1,Nb); ...
         x3 zeros(1,Nb)  ze zeros(1,Nb) -x1 zeros(1,Nb); ...
        -x2 zeros(1,Nb)  x1 zeros(1,Nb)  ze zeros(1,Nb)];  

    A=[-AS AU AOm; ...
        AF zeros(3,6); ...
        AM zeros(3,6)];
    
    % rhs assembly
    rhs = [0.5*v+AT*v; zeros(6,1)];

    % solve and extract F, U, Omega
    sol=A\rhs;
    F=-sol(1:3*N);
    U=sol(3*N+1:3*N+3)
    Om=sol(3*N+4:3*N+6)

    dz(1:3,1)=U;
    dz(4:6,1)=cross(Om,b1);
    dz(7:9,1)=cross(Om,b2);
   
    if ~isempty(varargin)
        dz(10:9+3*N,1)=F;
    end
    
    [x1,x2,x3]=ExtractComponents(xs);ze=0*x1;
    
    u=[kron(eye(3),ones(Ns,1))*U+[ze -x3 x2; x3 ze -x1; -x2 x1 ze]*Om+vs;
       vb]; % surface velocity on force points (swimmer and boundary)
    w=repmat(sum(NN(1:Q,1:N),1)',3,1);
    W=dot(u,F.*w); % calculate rate of working, weighting from quadrature points
