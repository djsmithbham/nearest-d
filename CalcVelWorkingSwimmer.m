function [Vel,W]=CalcVelWorkingSwimmer(problem,blockSize)

Vel=[problem.sol.X0(end,:)-problem.sol.X0(1,:)]/(problem.sol.t(end)-problem.sol.t(1));

% rate of working requires force and velocity at each quadrature point on
% the swimmer

% trapezium rule
ww=zeros(length(problem.sol.t)-1,1);
for m=1:length(problem.sol.t)-1
    dt(m)=problem.sol.t(m+1)-problem.sol.t(m);
    ww(m)=0.5*(sum(problem.sol.w(m,:).*  problem.sol.F(m,:).*  problem.sol.u(m,:))+...
               sum(problem.sol.w(m+1,:).*problem.sol.F(m+1,:).*problem.sol.u(m+1,:)));
end
W=dot(ww,dt);