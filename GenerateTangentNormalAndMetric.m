function [TGT1,TGT2,NML,dS]=GenerateTangentNormalAndMetric(X,nlocalpts,Xint)

% approximates surface normal and metric from a set of points X
% input: X            - vector of length 3Q, set of points, ordered as 
%                       all x1 compts, then all x2, then all x3
%        nlocalpts    - integer, number of points used for PCA approximation of
%                       neighbourhood (9 is a good choice)
%        Xint         - set of interior points for determination of
%                       outward direction; simplest choice is [0;0;0];
% output: TGT1,TGT2,  - Qx3 matrices with vectors of tangents and 
%         NML           inward-pointing unit
%                       normal
%         dS          - vector of length Q, surface area associated to each
%                       point

[X1,X2,X3]=ExtractComponents(X);
[Xint1,Xint2,Xint3]=ExtractComponents(Xint);

j=1:nlocalpts;j=j(:);
for q=1:length(X1)
    r=(X1(q)-X1).^2+(X2(q)-X2).^2+(X3(q)-X3).^2;
    [rclose,iclose]=mink(r,length(j));
    rstore{q}=rclose;
    X1L=X1(iclose)-X1(iclose(1));
    X2L=X2(iclose)-X2(iclose(1));
    X3L=X3(iclose)-X3(iclose(1));
    XX{q}=[X1L,X2L,X3L];
    coeff=pca(XX{q});
    TGT1(q,:)=coeff(:,1)/norm(coeff(:,1));
    TGT2(q,:)=coeff(:,2)/norm(coeff(:,2));
    Ntemp=cross(coeff(:,1),coeff(:,2));
    Ntemp=Ntemp'/norm(Ntemp);
    % ensure inward pointing - find closest point in Xint
    rintsq=(X1(q)-Xint1).^2+(X2(q)-Xint2).^2+(X3(q)-Xint3).^2;  
    [~,intclose]=min(rintsq);
    XXint=[X1(q)-Xint1(intclose),X2(q)-Xint2(intclose),X3(q)-Xint3(intclose)];
    proj(q)=dot(XXint,Ntemp);
    NML(q,:)=-Ntemp*(1-2*(proj(q)<0));
    % approximate surface metric
    XXa=(XX{q}(2,:)-XX{q}(1,:))'; % vector to closest point
    Xah{q}=repmat(XXa'/norm(XXa),length(j),1);
    XX2{q}=repmat(dot(XX{q},Xah{q},2),1,3).*Xah{q}; % remove component along XXa 
    [rc2{q},ic2{q}]=min(XX2{q}(2:end,1).^2+XX2{q}(2:end,2).^2+XX2{q}(2:end,3).^2); % find closest point projected onto XXa, avoiding first entry
    XXb=XX{q}(ic2{q}+1,:);
    dS(q)=norm(cross(XXa,XXb));
end
