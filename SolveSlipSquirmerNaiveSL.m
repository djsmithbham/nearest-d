function [U,Om,F,us,w,W]=SolveSlipSquirmerNaiveSL(x,X,usl,epsilon,domain,blockSize)
% squirming spherical swimmer, one mode, single layer only (naive)
%
% inputs:
%   x = force/collocation points
%   X = quadrature points
%   usl = slip velocity magnitude (direction is towards north pole)
%   epsilon, domain, blockSize as usual
% outputs:
%   U, Om = body frame translational and angular velocity
%   F = force exerted by the nearest neighbours of each force point (=-f)
%   w = weighting (number of quadrature points neighbouring a given force point
%   W = rate of working

M=length(x)/3;
Q=length(X)/3;

% normal and metric on quadrature pts, using nlocal pts
% [0,0,0] is interior point for determining inside/outside
%[~,~,NML,dS]=GenerateTangentNormalAndMetric(X,nlocalpts,[0,0,0]);  

[Theta,tNth]=CalcPolarAngleSphere(x); % calculate polar angle and vector to north pole for slip velocity

[AS,NN]=AssembleStokesletMatrix(x,X,x,...
                epsilon,domain,blockSize); % single layer potential            
AF=[sum(NN(1:Q,:),1);sum(NN(Q+1:2*Q,:),1);sum(NN(2*Q+1:3*Q,:),1)]; % force summation
AU=-kron(eye(3),ones(M,1)); % translation of frame including double layer

[x1,x2,x3]=ExtractComponents(x);ze=0*x1; % component of velocity due to rotation of frame about x0
AOm=[ze -x3 x2; x3 ze -x1; -x2 x1 ze]; % rotation of frame including double layer
[x1,x2,x3]=ExtractComponents(X'*NN);ze=0*x1; % moment summation
AM=[ze -x3 x2; x3 ze -x1; -x2 x1 ze];

% assemble left hand side matrix
A = [-AS AU AOm; ...
      AF zeros(3,6); ...
      AM zeros(3,6)];

% assemble rhs
ubf=usl*repmat(sin(Theta),3,1).*tNth;
rhs = [ubf; zeros(6,1)]; % slip velocity magnitude is usl*sin(Theta), direction is towards north pole
sol=A\rhs;

F =-sol(    1:3*M);
U = sol(3*M+1:3*M+3);
Om= sol(3*M+4:3*M+6);

us=zeros(3*M,1);
us(    1:  M) = ubf(    1:  M)+U(1)+Om(2)*x3(:)-Om(3)*x1(:);
us(  M+1:2*M) = ubf(  M+1:2*M)+U(2)+Om(3)*x1(:)-Om(1)*x3(:);
us(2*M+1:3*M) = ubf(2*M+1:3*M)+U(3)+Om(1)*x2(:)-Om(2)*x1(:);

[x1,x2,x3]=ExtractComponents(x);ze=0*x1;
u=kron(eye(3),ones(M,1))*U+[ze -x3 x2; x3 ze -x1; -x2 x1 ze]*Om+ubf; % surface velocity on force points - this may need modifying
w=repmat(sum(NN(1:Q,1:M),1)',3,1);
W=dot(u,F.*w); % calculate rate of working, weighting from quadrature points
    
