# NEAREST-D

Nearest neighbour regularized stokeslet method including double layer potential

To produce a full set of results (will take several hours), execute the command:

main_AllResultsForPaper

to avoid having to re-run computations, type e.g.

load figure1.mat 

then run the section of the relevant script that produces the plot only

