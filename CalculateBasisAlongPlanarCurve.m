function [tangVec,normVec,binormVec]=CalculateBasisAlongPlanarCurve(s,x,y)

m=size(x,1);

ds=(s(3:m,:)-s(1:m-2,:))*0.5;
ds=[s(2,:)-s(1,:); ds; s(m,:)-s(m-1,:)];
dx=(x(3:m,:)-x(1:m-2,:))*0.5;
dx=[x(2,:)-x(1,:); dx; x(m,:)-x(m-1,:)];
dy=(y(3:m,:)-y(1:m-2,:))*0.5;
dy=[y(2,:)-y(1,:); dy; y(m,:)-y(m-1,:)];

tx=dx./ds;
ty=dy./ds;
tz=0*dx;

tmag=sqrt(tx.^2+ty.^2);

tangVec.x=tx./tmag;
tangVec.y=ty./tmag;
tangVec.z=tz;

normVec.x=-tangVec.y;
normVec.y=tangVec.x;
normVec.z=0*dy;

binormVec.x=0*tangVec.x;
binormVec.y=0*tangVec.y;
binormVec.z=0*tangVec.z+1;
