function [FF,F,w,NN]=SolveSingleLayerNoSlipResistance(x,X,U,ep,domain,blockSize)

% F is stokeslet distribution (i.e. -f)

M=length(x)/3;
Q=length(X)/3;

[AS,NN]=AssembleStokesletMatrix(x,X,x,ep,domain,blockSize);
u=kron(U,ones(M,1));

F=AS\u;

w=sum(NN,1);

[F1,F2,F3]=ExtractComponents(NN*F);
FF=[sum(F1);sum(F2);sum(F3)];
