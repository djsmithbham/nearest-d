function problem=SolveSwimmingTrajectoryAndForcesDL(problem,blockSize,nlocalpts,varargin)

% Solves problem of swimmer trajectory and accompanying time-varying forces
% for velocity field reconstruction, includes double layer potential
%
% input:
%   x00        Initial position vector
%   b10, b20   Initial basis vectors (third basis vector is calculated via
%              cross product)
%   tRange     time range over which to calculate trajectory
%   swimmer    Structure describing how to calculate swimmer shape and
%              kinematics
%   boundary   Structure describing boundary
%   epsilon    regularisation parameter
%   domain     switch for use of image systems
%              'i' = use infinite fluid (usual)
%              'h' = use half space images (Ainley/Blakelet)
%   blockSize  memory size for stokeslet matrix block assembly in GB.
%              0.2 is a safe choice.
%   varargin   if varargin{1}='f' then includes force components
%              if varargin{2} nonempty then uses Runge Kutta with specified fixed
%              step

% outputs: augments swimmer structure with velocity and force

x00=problem.x00;
b10=problem.b10;
b20=problem.b20;
tRange=problem.tRange;
swimmer=problem.swimmer;
boundary=problem.boundary;
epsilon=problem.epsilon;
domain=problem.domain;

[xis,~,~] = swimmer.fn(0,swimmer.model);
if isempty(boundary)
    xib=[];
else
    [xib,~]   = boundary.fn(boundary.model);
end

DOF=length(xis)+length(xib);
z0=[x00;b10;b20;zeros(DOF,1)];
if length(varargin)>1
    'using RK'
    [t,z]=odeRK(@(t,z) SolveSwimmingProblemWithBoundaryDL(z,swimmer,boundary,t,epsilon,domain,blockSize,nlocalpts,varargin),tRange,z0,varargin{2});    
else
    'using ode45'
    [t,z]=ode45(@(t,z) SolveSwimmingProblemWithBoundaryDL(z,swimmer,boundary,t,epsilon,domain,blockSize,nlocalpts,varargin),tRange,z0);
end
problem.sol.t=t;
X0=zeros(length(t),3);
B1=zeros(length(t),3);
B2=zeros(length(t),3);
B3=zeros(length(t),3);
U=zeros(length(t),3);
dB1=zeros(length(t),3);
dB2=zeros(length(t),3);
dB3=zeros(length(t),3);
Om=zeros(length(t),3);
F=zeros(length(t),DOF);
x=zeros(length(t),DOF);
u=zeros(length(t),DOF);
for m=1:length(t)
    [U(m,:),X0(m,:)]  =ExtractRateOfChangeOfSolution(t,z(:,1:3),t(m));
    [dB1(m,:),B1(m,:)]=ExtractRateOfChangeOfSolution(t,z(:,4:6),t(m));
    [dB2(m,:),B2(m,:)]=ExtractRateOfChangeOfSolution(t,z(:,7:9),t(m));
    B3(m,:)=cross(B1(m,:),B2(m,:));
    dB3(m,:)=cross(dB1(m,:),B2(m,:))+cross(B1(m,:),dB2(m,:));
    Om(m,:)=dot(dB2(m,:),B3(m,:))*B1(m,:)-dot(dB1(m,:),B3(m,:))*B2(m,:)+dot(dB1(m,:),B2(m,:))*B3(m,:);
    B{m}=[B1(m,:)' B2(m,:)' B3(m,:)'];

    [xi,vi,Xi,xCL]=swimmer.fn(t(m),swimmer.model);
    xr=ApplyRotationMatrix(B{m},xi);
    [xr1,xr2,xr3]=ExtractComponents(xr);

    xs=TranslatePoints(xr,X0(m,:)');
    vs=ApplyRotationMatrix(B{m},vi);
    [vs1,vs2,vs3]=ExtractComponents(vs);
    
    Xs=ApplyRotationMatrix(B{m},Xi);
    Xs=TranslatePoints(Xs,X0(m,:)');
    
    NNss=NearestNeighbourMatrix(Xs,xs,blockSize);
    
    % normal and metric
    xxCL=ApplyRotationMatrix(B{m},xCL); % interior points (centreline) for double layer
    Xint=TranslatePoints(xxCL,X0(m,:)');
    [~,~,NML,dS]=GenerateTangentNormalAndMetric(Xs,nlocalpts,Xint);
    
    if isempty(boundary)
        xb=[];
        Xb=[];
        vb=[];
        NNbb=sparse([]);
    else
        [xb,Xb]=boundary.fn(boundary.model);
        vb=xb*0; % boundary is stationary
        NNbb=NearestNeighbourMatrix(Xb,xb,blockSize);
    end
    
    xx=MergeVectorGrids(xs,xb);
    XX=MergeVectorGrids(Xs,Xb);
    vv=MergeVectorGrids(vs,vb);
    
    NN=MergeNNMatrices(NNss,NNbb); % assemble nearest-neighbour matrices separately for swimmer and boundary
    
    if ~exist('w') % creates w on first step
        w=zeros(length(t),length(xx));
    end
    w(m,:)=sum(NN,1); % weights from number of nearest points
    
    uus1=vs1+U(m,1)+Om(m,2)*xr3-Om(m,3)*xr2; % swimmer frame movement
    uus2=vs2+U(m,2)+Om(m,3)*xr1-Om(m,1)*xr3;
    uus3=vs3+U(m,3)+Om(m,1)*xr2-Om(m,2)*xr1;
    uus=[uus1;uus2;uus3];
    
    uub=vb; % boundary is not moving
    
    uu=MergeVectorGrids(uus,uub);
    
    x(m,:)=xx';
    if ~exist('X') % creates X on first step
        X=zeros(length(t),length(XX));
    end
    X(m,:)=XX';
    u(m,:)=uu'; % velocity of swimmer and boundary in labframe
    
    [F(m,:),~]=ExtractRateOfChangeOfSolution(t,z(:,10:end),t(m));
    
    if ~exist('nml') % creates nml on first step
        nml=zeros(length(t),length(Xs));
    end
    nml(m,:)=NML(:)';
    ds(m,:)=dS(:)';
end
problem.sol.u=u;
problem.sol.x=x;
problem.sol.X=X;
problem.sol.w=w;
problem.sol.n=nml;
problem.sol.dS=ds;
problem.sol.F=F;
problem.sol.U=U;
problem.sol.Om=Om;
problem.sol.X0=X0;
problem.sol.B=B;
