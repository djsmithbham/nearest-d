function [FF,F,us,w,NN]=SolveDoubleLayerSlipResistance(x,X,U,ep,domain,blockSize,nlocalpts)

% F is stokeslet distribution (=-f)
% us is surface velocity distribution

M=length(x)/3;
Q=length(X)/3;

[tgt1,tgt2,nml,~]=GenerateTangentNormalAndMetric(x,nlocalpts,[0,0,0]);
[~,~,NML,dS]=GenerateTangentNormalAndMetric(X,nlocalpts,[0,0,0]);  

[AS,~]=AssembleStokesletMatrix(x,X,x,ep,domain,blockSize);
[AT,NN]=AssembleDoubleLayerMatrix(x,X,x,NML,dS,ep,domain,blockSize,1);
u=kron(U,ones(M,1));

tgt1mat=[diag(tgt1(1:M)) diag(tgt1(M+1:2*M)) diag(tgt1(2*M+1:3*M))];
tgt2mat=[diag(tgt2(1:M)) diag(tgt2(M+1:2*M)) diag(tgt2(2*M+1:3*M))];
tgtmat=[tgt1mat; tgt2mat];
nmlmat=[diag(nml(1:M)) diag(nml(M+1:2*M)) diag(nml(2*M+1:3*M))];

% quick solution just for force
%rhs=[nmlmat*u; zeros(2*M,1)];
%A=[-nmlmat*inv(0.5*eye(3*M)+AT)*AS; tgtmat];   % slightly over-complicated and we need u on surface anyway
%f=A\rhs;

A = [ AS,          -0.5*eye(3*M)-AT; ...      % AS not -AS because of sign on F_k
      zeros(M,3*M), nmlmat          ; ...
      tgtmat      , zeros(2*M,3*M) ];
rhs=[zeros(3*M,1) ; ...
      nmlmat*u     ; ...
      zeros(2*M,1)];
sol=A\rhs;

F=sol(1:3*M);
us=sol(3*M+1:6*M);
w=sum(NN,1);

[F1,F2,F3]=ExtractComponents(NN*F);
FF=[sum(F1);sum(F2);sum(F3)];
