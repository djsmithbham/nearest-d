
a=1;
m=10;
q=40;
U=[0;0;-1];
ep=0.01;
domain='i';
blockSize=0.2;
nlocalpts=9;

[x,X]=GenerateSpherePoints(m,q,a);
N=length(x)/3;

[FF,F,w,NN]=SolveSingleLayerNoSlipResistance(x,X,U,ep,domain,blockSize);

figure(1);clf;
[x1,x2,x3]=ExtractComponents(x);
[X1,X2,X3]=ExtractComponents(X);

nx=14;nz=15;
xmin=-2;xmax=2;
zmin=-2;zmax=2;
yval=0;

x1g=linspace(xmin,xmax,nx);
x3g=linspace(zmin,zmax,nz);
[xx1g,xx3g]=ndgrid(x1g,x3g);
xx2g=0*xx1g+yval;

xx1g=xx1g(:);
xx2g=xx2g(:);
xx3g=xx3g(:);

xg=[xx1g;xx2g;xx3g];

AS=AssembleStokesletMatrix(xg,X,x,ep,domain,blockSize);

u=AS*F;

[u1,u2,u3]=ExtractComponents(u); 

% exclude interior and surface of sphere
u1=u1.*(xx1g.^2+xx2g.^2+xx3g.^2>a*(1+ep));
u2=u2.*(xx1g.^2+xx2g.^2+xx3g.^2>a*(1+ep));
u3=u3.*(xx1g.^2+xx2g.^2+xx3g.^2>a*(1+ep));

%%
% load figure2a.mat

quiver3(xx1g,xx2g,xx3g,u1,u2,u3,0.6,'b');
axis equal;hold on;
hp=plot3(x1,x2,x3,'k.');set(hp,'markersize',3);
hq=plot3(X1,X2,X3,'m.');set(hq,'markersize',1);
nth=50;nphi=100;
[xs,ys,zs]=GenerateSphereSurfaceForVisualisation(nth,nphi,a);
surf(xs,ys,zs,0*zs);shading flat;light;lighting phong;
%view([38 12]);
view([23 8]);

wd=8;ht=8;
set(gcf,'paperunits','centimeters');
set(gcf,'papersize',[wd ht]);
set(gcf,'paperposition',[0 0 wd ht]);
print(gcf,'-dpdf','-r600','noSlipField.pdf');
