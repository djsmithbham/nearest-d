% resistance problem for sphere: sweeps over partial slip cases

clear all;

a=1;
m=20;
q=60;

lambda=2.^[-8:0.5:8];

U=[1;0;0];
ep=0.01;
domain='i';
blockSize=0.2;
nlocalpts=9;

[x,X]=GenerateSpherePoints(m,q,a);
hq=CalcDiscr_h(X,blockSize);
hf=CalcDiscr_h(x,blockSize);
Dof=length(x);
NM=length(x)/3;
NQ=length(X)/3;

%%
        
for iL=1:length(lambda)
    ['lambda = ', num2str(lambda(iL))]        
    F{iL}=SolveDoubleLayerPartialSlipResistance(x,X,U,lambda(iL),ep,domain,blockSize,nlocalpts);
    FExact{iL}=6*pi*a*U*(1+2*lambda(iL)/a)/(1+3*lambda(iL)/a);
    err(iL)=norm(F{iL}-FExact{iL})/norm(FExact{iL});
end

%%
% for comparison, single layer only version
for iL=1:length(lambda)
    ['lambda = ', num2str(lambda(iL))]        
    FSL{iL}=SolveSingleLayerPartialSlipResistance(x,X,U,lambda(iL),ep,domain,blockSize,nlocalpts);
    FExact{iL}=6*pi*a*U*(1+2*lambda(iL)/a)/(1+3*lambda(iL)/a);
    errSL(iL)=norm(FSL{iL}-FExact{iL})/norm(FExact{iL});
end

%%
% load figure3.mat

wd=13;ht=7;
opts.fig=1;
opts.fs=9;
opts.fn='times';
     
Fnum=cell2mat(F);
FSLnum=cell2mat(FSL);

figure(opts.fig);clf;
semilogx(lambda,Fnum(1,:),'r.');
hold on;
hs=semilogx(lambda,FSLnum(1,:),'ko');
set(hs,'markersize',2);
semilogx(lambda,6*pi*a*(1+2*lambda/a)./(1+3*lambda/a),'b-');
hx=xlabel('slip length \(\lambda\)','interpreter','latex');set(hx,'fontsize',opts.fs,'fontname',opts.fn);
hy=ylabel('drag component \(F_1\)','interpreter','latex');set(hx,'fontsize',opts.fs,'fontname',opts.fn);
set(gca,'fontsize',opts.fs,'fontname',opts.fn);
box on;
set(gca,'tickdir','out');
hl=legend('computed drag (full boundary integral)','computed drag (single layer only)','exact drag');
set(hl,'box','off');
set(opts.fig,'paperunits','centimeters','papersize',[wd ht],'paperposition',[0 0 wd ht]);
print(opts.fig,'-r600','-dpdf','partialSlipResistance.pdf');
