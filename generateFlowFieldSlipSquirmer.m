% generateFlowFieldSlipSquirmer.m

a=1;
m=20;
q=40;
ep=0.01;
domain='i';
blockSize=0.2;
nlocalpts=9;
usl=3/2;

[x,X]=GenerateSpherePoints(m,q,a);

Dof=length(x);
hq=CalcDiscr_h(X,blockSize);

[U,Om,F,us,w,W]=SolveSlipSquirmerDL(x,X,usl,ep,domain,blockSize,nlocalpts);

figure(1);clf;
[x1,x2,x3]=ExtractComponents(x);
[X1,X2,X3]=ExtractComponents(X);

nx=14;nz=15;
xmin=-2;xmax=2;
zmin=-2;zmax=2;
yval=0;

x1g=linspace(xmin,xmax,nx);
x3g=linspace(zmin,zmax,nz);
[xx1g,xx3g]=ndgrid(x1g,x3g);
xx2g=0*xx1g+yval;

xx1g=xx1g(:);
xx2g=xx2g(:);
xx3g=xx3g(:);

xg=[xx1g;xx2g;xx3g];

[~,~,NML,dS]=GenerateTangentNormalAndMetric(X,nlocalpts,[0,0,0]);  
AS=AssembleStokesletMatrix(xg,X,x,ep,domain,blockSize);
[AT,~]=AssembleDoubleLayerMatrix(xg,X,x,NML,dS,ep,domain,blockSize,0);

u=AS*F-AT*us;

[u1,u2,u3]=ExtractComponents(u);

% exclude interior and surface of sphere
u1=u1.*(xx1g.^2+xx2g.^2+xx3g.^2>a*(1+ep));
u2=u2.*(xx1g.^2+xx2g.^2+xx3g.^2>a*(1+ep));
u3=u3.*(xx1g.^2+xx2g.^2+xx3g.^2>a*(1+ep));

%%
fs=9;
fn='times';

%load figure4a.mat

quiver3(xx1g,xx2g,xx3g,u1,u2,u3,0.8,'b')
axis equal;hold on;
hp=plot3(x1,x2,x3,'k.');set(hp,'markersize',3);
hp=plot3(X1,X2,X3,'m.');set(hp,'markersize',1);
set(gca,'fontsize',fs,'fontname',fn);
nth=50;nphi=100;
[xs,ys,zs]=GenerateSphereSurfaceForVisualisation(nth,nphi,a);
hx=xlabel('\(x_1\)','interpreter','latex');
hy=ylabel('\(x_2\)','interpreter','latex');
hz=zlabel('\(x_3\)','interpreter','latex');
set(hx,'fontsize',fs,'fontname',fn);
set(hy,'fontsize',fs,'fontname',fn);
set(hz,'fontsize',fs,'fontname',fn);
surf(xs,ys,zs,sin(acos(zs)));shading flat;light;lighting phong;
hc=colorbar;
set(hc,'fontsize',fs,'fontname',fn);
%view([38 12]);
view([23 8]);

wd=10;ht=8;
set(gcf,'paperunits','centimeters');
set(gcf,'papersize',[wd ht]);
set(gcf,'paperposition',[0 0 wd ht]);
print(gcf,'-dpdf','-r600','slipSwimmerField.pdf');

