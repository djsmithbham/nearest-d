function PlotSwimmerFlow(problem,nt,blockSize,opts)

% plots swimmer and flow field from already-calculated swimmer.w, f, u, x, X

figure(opts.fig);clf;hold on;
[x,y,z]=ExtractComponents(problem.sol.x(nt,:));
[X,Y,Z]=ExtractComponents(problem.sol.X(nt,:));
if ~isempty(opts.forcecolor)
    hf=plot3(x,y,z,'.');set(hf,'markersize',opts.forcemarker,'color',opts.forcecolor);
end
if ~isempty(opts.quadcolor)
    hq=plot3(X,Y,Z,'.');set(hq,'markersize',opts.quadmarker,'color',opts.quadcolor);
end
if ~isempty(opts.surfvelcolor)
    [u,v,w]=ExtractComponents(problem.sol.u(nt,:)*opts.surfvelscale);
    hs=quiver3(x,y,z,u,v,w,0);set(hs,'color',opts.surfvelcolor);
end
if ~isempty(opts.trajcolor)
    X0=problem.sol.X0;
    plot3(X0(:,1),X0(:,2),X0(:,3),opts.trajcolor);
end
if ~isempty(opts.normcolor)
    nml=problem.sol.n;
    [n1,n2,n3]=ExtractComponents(nml(nt,:));
    hn=quiver3(X,Y,Z,-n1,-n2,-n3);set(hn,'color',opts.normcolor);
end
if ~isempty(opts.solidsurf)
    [xbf,ybf,zbf]=UndulatoryModelFiniteThicknessForVisualisation(problem.sol.t(nt),problem.swimmer.model);
    b1=problem.sol.B{nt}(:,1);
    b2=problem.sol.B{nt}(:,2);
    b3=problem.sol.B{nt}(:,3);
    x=xbf*b1(1)+ybf*b2(1)+zbf*b3(1)+problem.sol.X0(nt,1);
    y=xbf*b1(2)+ybf*b2(2)+zbf*b3(2)+problem.sol.X0(nt,2);
    z=xbf*b1(3)+ybf*b2(3)+zbf*b3(3)+problem.sol.X0(nt,3);
    hs=surf(x,y,z,0*z);shading flat;light;
    set(hs,'faceAlpha',opts.alpha);
end
if ~isempty(opts.solidbdry)
    pbm=problem.boundary.model;
    z1=pbm.O(3);
    z2=pbm.O(3)+pbm.h;
    xb=pbm.O(1)+linspace(-0.5*pbm.Lx,0.5*pbm.Lx,pbm.Nx);
    yb=pbm.O(2)+linspace(-0.5*pbm.Ly,0.5*pbm.Ly,pbm.Ny);
    [Xb,Yb]=ndgrid(xb,yb);
    Z1=z1+0*Xb;
    Z2=z2+0*Xb;
    hs1=surf(Xb,Yb,Z1,0*Z1);
    hs2=surf(Xb,Yb,Z2,0*Z2);
    set(hs1,'faceAlpha',opts.alphabdry);
    set(hs2,'faceAlpha',opts.alphabdry);
end
if ~isempty(opts.fieldcolor)
    xg=linspace(opts.axis(1),opts.axis(2),opts.ng(1));
    yg=linspace(opts.axis(3),opts.axis(4),opts.ng(2));
    if opts.ng(1)==1
        xg=(opts.axis(1)+opts.axis(2))*0.5;
    else
        xg=xg+(xg(2)-xg(1))*0.5;xg(end)=[];
    end
    if opts.ng(2)==1
        yg=(opts.axis(3)+opts.axis(4))*0.5;
    else
        yg=yg+(yg(2)-yg(1))*0.5;yg(end)=[];
    end
    if opts.ng(3)==1
        zg=(opts.axis(5)+opts.axis(6))*0.5;
    else
        zg=linspace(opts.axis(5),opts.axis(6),opts.ng(3));
        zg=zg+(zg(2)-zg(1))*0.5;zg(end)=[];
    end    
    [xxg,yyg]=meshgrid(xg,yg);
    for nz=1:length(zg)
        zzg=0*xxg+zg(nz);
        hu=plot3(xxg,yyg,zzg,'.');set(hu,'color',opts.fieldcolor);
        xx=[xxg(:);yyg(:);zzg(:)];
        X=problem.sol.X(nt,:);X=X(:);
        x=problem.sol.x(nt,:);x=x(:);
        F=problem.sol.F(nt,:);F=F(:);
        NML=problem.sol.n(nt,:);NML=reshape(NML,[],3);
        dS=problem.sol.dS(nt,:);
        u=problem.sol.u(nt,:);u=u(:);
        if size(NML,1)<length(X)/3 % detects if boundary present
            Ns=size(NML,1);
            Nb=size(X,1)/3-Ns;
            NML=[NML;zeros(Nb,3)]; % double layer on stationary boundary has zero contribution so this can be zero
            dS=[dS zeros(1,Nb)];
        end
        [AS,~]=AssembleStokesletMatrix(xx,X,x,problem.epsilon,problem.domain,blockSize);
        [AT,~]=AssembleDoubleLayerMatrix(xx,X,x,NML,dS,problem.epsilon,problem.domain,blockSize,0);
        uu=AS*F-AT*u;
        [ug,vg,wg]=ExtractComponents(uu*opts.fieldscale);
        hu=quiver3(xxg(:),yyg(:),zzg(:),ug,vg,wg,0);set(hu,'color',opts.fieldcolor);
    end
end
axis equal;
axis(opts.axis);
box on;
if ~isempty(opts.angle)
    view(opts.angle)
end
set(gca,'tickdir','out');
set(gca,'fontsize',opts.fs,'fontname',opts.fn);


