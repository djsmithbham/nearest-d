% carries out sweep of undulatory swimmer aspect ratios to find vel and RoW

clear all;

% for setting up beat interpolant
nbeats=3;
tRange=[0 nbeats];
nns=40;
dt=0.02;

% numerical parameters
epsilon=0.01;
domain='i';
blockSize=0.2;
nlocalpts=9;

xyWaveFn=@UndulatorySwimmer;
args.phase=0;
args.k=2*pi;
s = linspace(0,1,nns);
t = tRange(1):dt:tRange(2);
[S,T]=ndgrid(s,t);

aVals=[0.02 0.025 0.03 0.035 0.04 0.045 0.05 0.055 0.06]; %0.06;

for r=1:length(aVals)
    ['aVal = ' num2str(aVals(r))]
    % set up swimmer, including discretisation parameters
    swimmer.fn=@UndulatoryModelFiniteThicknessIncludedPts;
    swimmer.xyWaveFn=@UndulatorySwimmer;
    swimmer.visFn=@UndulatoryModelFiniteThicknessForVisualisation;
    swimmer.model.F=ConstructInterpolantFromxyForm(S,T,xyWaveFn,args);
    swimmer.model.arad=aVals(r); % radius multiplier for finite thickness
    swimmer.model.thicknessFn= @PowerRad; % @ConstRad; %
    swimmer.model.ns=20;
    swimmer.model.nth=6;
    swimmer.model.Ns=40;
    swimmer.model.Nth=12;

    problem{r}.x00=[0;0;0];
    B=RotationMatrix(0*pi/3,3);
    problem{r}.b10=B(:,1);
    problem{r}.b20=B(:,2);
    problem{r}.tRange=[1 2];

    % no boundary
    boundary=[];    

    problem{r}.swimmer=swimmer;
    problem{r}.boundary=boundary;

    problem{r}.epsilon=epsilon;
    problem{r}.domain=domain;

    problem{r}=SolveSwimmingTrajectoryAndForcesSL(problem{r},blockSize,'f',0.02);
    
    [vv,ww]=CalcVelWorkingSwimmer(problem{r},blockSize);
    RoWSL(r)=ww;
end
