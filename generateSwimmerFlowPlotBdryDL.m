% generates flow plots for undulatory swimmer, no boundary

clear all;

% for setting up beat interpolant
nbeats=7;
tRange=[0 nbeats];
nns=40;
dt=0.02;

% numerical parameters
epsilon=0.01;
domain='i';
blockSize=0.2;
nlocalpts=9;

xyWaveFn=@UndulatorySwimmer;
args.phase=0;
args.k=2*pi;
s = linspace(0,1,nns);
t = tRange(1):dt:tRange(2);
[S,T]=ndgrid(s,t);

aVals=[0.02 0.06];

for r=1:length(aVals)
    % set up swimmer, including discretisation parameters
    swimmer.fn=@UndulatoryModelFiniteThicknessIncludedPts;
    swimmer.model.F=ConstructInterpolantFromxyForm(S,T,xyWaveFn,args);
    swimmer.model.arad=aVals(r); % radius multiplier for finite thickness
    swimmer.model.thicknessFn=@PowerRad; % @ConstRad; %
    swimmer.model.ns=20;
    swimmer.model.nth=6;
    swimmer.model.Ns=40;
    swimmer.model.Nth=12;

    problem{r}.x00=[0;0;0];
    B=RotationMatrix(0*pi/3,3);
    problem{r}.b10=B(:,1);
    problem{r}.b20=B(:,2);
    problem{r}.tRange=[1 2];

    boundary.fn=@PlaneBoundary2;
    boundary.model.h=0.5;
    boundary.model.nx=21;
    boundary.model.ny=20;
    boundary.model.Nx=41;
    boundary.model.Ny=40;
    boundary.model.Lx=2;
    boundary.model.Ly=2;
    boundary.model.O=[0 0 -0.25];    

    problem{r}.swimmer=swimmer;
    problem{r}.boundary=boundary;

    problem{r}.epsilon=epsilon;
    problem{r}.domain=domain;

    problem{r}=SolveSwimmingTrajectoryAndForcesDL(problem{r},blockSize,nlocalpts,'',0.02);%
end

%save undulatoryNoBoundaryForPlotsBdryDL.mat
%%
%load figure10_11.mat
wd=6.5;ht=6;
nt=1;

for r=1:length(aVals)
    opts.fig=1;
    opts.fs=10;
    opts.fn='times';
    opts.axis=[-0.1 0.9 -0.6 0.4 -0.5 0.5];
    opts.forcemarker=4;
    opts.quadmarker=2;
    opts.forcecolor='r';
    opts.quadcolor='k';
    opts.surfvelcolor='k';
    opts.solidsurf='y';
    opts.alpha=1;
    opts.solidbdry=[];
    opts.surfvelscale=0.25;
    opts.fieldscale=0.25;
    opts.fieldcolor='b';
    opts.ng=[10 12 1];
    opts.trajcolor=[];
    opts.normcolor=[];
    opts.angle=[];
    nt=min(find(problem{r}.sol.t>=1));
    PlotSwimmerFlow(problem{r},nt,blockSize,opts);
    set(opts.fig,'paperunits','centimeters','papersize',[wd ht],'paperposition',[0 0 wd ht]);
    print(opts.fig,'-r600','-dpng',['swimmerPlotBdryDL_' num2str(problem{r}.sol.t(nt)) '_' num2str(aVals(r)) '.png']);
    nt=min(find(problem{r}.sol.t>=1.2));
    PlotSwimmerFlow(problem{r},nt,blockSize,opts);
    set(opts.fig,'paperunits','centimeters','papersize',[wd ht],'paperposition',[0 0 wd ht]);
    print(opts.fig,'-r600','-dpng',['swimmerPlotBdryDL_' num2str(problem{r}.sol.t(nt)) '_' num2str(aVals(r)) '.png']);
    nt=min(find(problem{r}.sol.t>=1.4));
    PlotSwimmerFlow(problem{r},nt,blockSize,opts);
    set(opts.fig,'paperunits','centimeters','papersize',[wd ht],'paperposition',[0 0 wd ht]);
    print(opts.fig,'-r600','-dpng',['swimmerPlotBdryDL_' num2str(problem{r}.sol.t(nt)) '_' num2str(aVals(r)) '.png']);
    nt=min(find(problem{r}.sol.t>=1.60));
    PlotSwimmerFlow(problem{r},nt,blockSize,opts);
    set(opts.fig,'paperunits','centimeters','papersize',[wd ht],'paperposition',[0 0 wd ht]);
    print(opts.fig,'-r600','-dpng',['swimmerPlotBdryDL_' num2str(problem{r}.sol.t(nt)) '_' num2str(aVals(r)) '.png']);
    nt=min(find(problem{r}.sol.t>=1.8));
    PlotSwimmerFlow(problem{r},nt,blockSize,opts);
    set(opts.fig,'paperunits','centimeters','papersize',[wd ht],'paperposition',[0 0 wd ht]);
    print(opts.fig,'-r600','-dpng',['swimmerPlotBdryDL_' num2str(problem{r}.sol.t(nt)) '_' num2str(aVals(r)) '.png']);

    opts.trajcolor='k';
    opts.surfvelcolor=[];
    opts.fieldcolor=[];
    opts.axis=[-0.3 0.9 -0.6 0.6 -0.5 0.5];
    nt=1;
    PlotSwimmerFlow(problem{r},nt,blockSize,opts);
    set(opts.fig,'paperunits','centimeters','papersize',[wd ht],'paperposition',[0 0 wd ht]);
    print(opts.fig,'-r600','-dpng',['swimmerPlotBdryDL_trajectory_' num2str(aVals(r)) '.png']);
end    

for r=1:length(aVals)
    opts.fig=1;
    opts.fs=10;
    opts.fn='times';
    opts.axis=[-0.1 0.9 -0.6 0.4 -0.25 0.25];
    opts.forcemarker=4;
    opts.quadmarker=2;
    opts.forcecolor=[];
    opts.quadcolor=[];
    opts.surfvelcolor=[];
    opts.solidsurf='y';
    opts.alpha=0.4;
    opts.solidbdry='y';
    opts.alphabdry=0.2;
    opts.surfvelscale=0.25;
    opts.fieldscale=0.25;
    opts.fieldcolor='b';
    opts.ng=[16 1 15];
    opts.trajcolor=[];
    opts.normcolor=[];
    opts.angle=[14 11];
    nt=min(find(problem{r}.sol.t>=1));
    PlotSwimmerFlow(problem{r},nt,blockSize,opts);
    set(opts.fig,'paperunits','centimeters','papersize',[wd ht],'paperposition',[0 0 wd ht]);
    set(gca,'ytick',[-0.6 -0.2 0.2]);camlight;
    print(opts.fig,'-r600','-dpng',['swimmerPlotIsoBdryDL_' num2str(problem{r}.sol.t(nt)) '_' num2str(aVals(r)) '.png']);
    nt=min(find(problem{r}.sol.t>=1.25));
    PlotSwimmerFlow(problem{r},nt,blockSize,opts);
    set(opts.fig,'paperunits','centimeters','papersize',[wd ht],'paperposition',[0 0 wd ht]);
    set(gca,'ytick',[-0.6 -0.2 0.2]);camlight;
    print(opts.fig,'-r600','-dpng',['swimmerPlotIsoBdryDL_' num2str(problem{r}.sol.t(nt)) '_' num2str(aVals(r)) '.png']);
    nt=min(find(problem{r}.sol.t>=1.5));
    PlotSwimmerFlow(problem{r},nt,blockSize,opts);
    set(opts.fig,'paperunits','centimeters','papersize',[wd ht],'paperposition',[0 0 wd ht]);
    set(gca,'ytick',[-0.6 -0.2 0.2]);camlight;
    print(opts.fig,'-r600','-dpng',['swimmerPlotIsoBdryDL_' num2str(problem{r}.sol.t(nt)) '_' num2str(aVals(r)) '.png']);
    nt=min(find(problem{r}.sol.t>=1.75));
    PlotSwimmerFlow(problem{r},nt,blockSize,opts);
    set(opts.fig,'paperunits','centimeters','papersize',[wd ht],'paperposition',[0 0 wd ht]);
    set(gca,'ytick',[-0.6 -0.2 0.2]);camlight;
    print(opts.fig,'-r600','-dpng',['swimmerPlotIsoBdryDL_' num2str(problem{r}.sol.t(nt)) '_' num2str(aVals(r)) '.png']);
end    

