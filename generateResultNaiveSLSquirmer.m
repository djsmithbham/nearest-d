% generateResultNaiveSLSquirmer

a=1;
m=20;
q=120;
ep=0.01;
domain='i';
blockSize=0.2;
usl=3/2;

tic;
[x,X]=GenerateSpherePoints(m,q,a);
[U,Om,F,~,w,W]=SolveSlipSquirmerNaiveSL(x,X,usl,ep,domain,blockSize);
errU=norm(U-[0;0;-1]);
errW=abs(W-12*pi)/(12*pi);
hq=CalcDiscr_h(X,blockSize);
hf=CalcDiscr_h(x,blockSize);
Dof=length(x);
NM=length(x)/3;
NQ=length(X)/3;
WT=toc;

% load naiveSLSquirmer

errU
errW
